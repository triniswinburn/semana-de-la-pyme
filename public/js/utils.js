        $('.caja-video').addClass('hidden');
        $(".caja-video").slice(0, 8).show();

        $('#arriba').click(function(){ //Id del elemento cliqueable
            $('html, body').animate({scrollTop:0}, 'slow');
            return false;
        });
        $('#listaregiones').change(function(){ //Id del elemento cliqueable
            $('.caja-video').addClass('hidden');
            $(".caja-video").hide();

            id_region = $(this).val();
            region = '.region'+id_region;
            if(id_region == '00'){
                $('.caja-video').removeClass('hidden');
            }else{
                $('.caja-video').addClass('hidden');
                $(region).removeClass('hidden');
            }
            
            if($('.caja-video').length == $('.hidden').length-1){                
                $('#notfound').removeClass('hidden');
            }else{
                $('#notfound').addClass('hidden');
            }
        });
        $('#load').click(function (e) {
                e.preventDefault();
                $(".caja-video:hidden").slice(0, 8).slideDown();
                if ($(".caja-video:hidden").length == 0) {
                    $("#load").fadeOut('slow');
                }
                $('html,body').animate({
                    scrollTop: $(this).offset().top
                }, 1500);
            });

        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.totop a').fadeIn();
            } else {
                $('.totop a').fadeOut();
            }
        });
