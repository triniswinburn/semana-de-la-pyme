<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request)  {
    $json = File::get("js/pymes.json");
    $videos = json_decode($json, true);
    $tmp = array();
    foreach($videos as $key => $value) {
        $tmp[] = array('k' => $key, 'v' => $value);
    }

    shuffle($tmp);
    $videos = array();
    foreach($tmp as $entry) {
        $videos[$entry['k']] = $entry['v'];
    }
    foreach($videos as $key => $value) {
        if($value['youtube']!="")
            $videos[$key]['youtube'] = explode( '/', $value['youtube'] )[4];
    }

    return view('welcome')->with(['videos'=> $videos, 'esconder'=> '', 'buscar'=> '', 'region'=>'']);
});
Route::get('/participantes', function (Request $request)  {
    $json = File::get("js/pymes.json");
    $videos = json_decode($json, true);
    $tmp = array();
    foreach($videos as $key => $value) {
        $tmp[] = array('k' => $key, 'v' => $value);
    }

    shuffle($tmp);
    $videos = array();
    foreach($tmp as $entry) {
        $videos[$entry['k']] = $entry['v'];
    }
    foreach($videos as $key => $value) {
        if($value['youtube']!="")
            $videos[$key]['youtube'] = explode( '/', $value['youtube'] )[4];
    }

    return view('welcome2')->with(['videos'=> $videos, 'esconder'=> '', 'buscar'=> '', 'region'=>'']);
});
Route::post('/participantes', function (Request $request) {
    $json = File::get("js/pymes.json");
    $videos = json_decode($json, true);

    foreach($videos as $key => $value) {
        if($value['youtube']!="")
            $videos[$key]['youtube'] = explode( '/', $value['youtube'] )[4];
    }

    $buscar = isset($request->buscar) ? $request->buscar : '-';
    
    if(isset($request->listaregiones)){
        if($request->listaregiones != "00"){
            foreach ($videos as $key => $value) {
                if($value['id_region'] != $request->listaregiones){
                    unset($videos[$key]);
                }
            }
        }
    }

    if(isset($request->buscar)){
        $cadena_buscada   = reemplazar($request->buscar);
        foreach ($videos as $key => $value) {
            $cadena_de_texto_nombre = reemplazar($value['nombre']);
            $cadena_de_texto_descripcion = reemplazar($value['descripcion']);
            $posicion_coincidencia = strpos($cadena_de_texto_nombre, $cadena_buscada);
            $posicion_coincidencia2 = strpos($cadena_de_texto_descripcion, $cadena_buscada);
            if($posicion_coincidencia === false && $posicion_coincidencia2 === false){
                unset($videos[$key]);
            }
        }
    }
    $tmp = array();
    foreach($videos as $key => $value) {
        $tmp[] = array('k' => $key, 'v' => $value);
    }
    shuffle($tmp);
    $videos = array();
    foreach($tmp as $entry) {
        $videos[$entry['k']] = $entry['v'];
    }
    $esconder = '';

    return view('welcome2')->with(['videos'=> $videos, 'esconder'=> $esconder, 'buscar'=> $buscar, 'region'=> $request->listaregiones]);
});
Route::get('/ranking', function ()  {
    $json = File::get("js/pymes.json");
    $aux = json_decode($json, true);
    $pymes = array();
    foreach ($aux as $key => $pyme) {
        $pymes[$pyme['btnlike']]['nombre'] = $pyme['nombre'];
        $pymes[$pyme['btnlike']]['region'] = $pyme['region'];
    }

    $json = File::get("js/votos.json");
    $votos = json_decode($json, true);
    foreach ($votos as $key => $voto) {
        $votos[$key]['nombre'] = $pymes[$voto['identifier']]['nombre'];
        $votos[$key]['region'] = $pymes[$voto['identifier']]['region'];
    }
    date_default_timezone_set("America/Santiago");
    $fecha = new DateTime();
    return view('ranking')->with(['pymes'=>$votos]);
});Route::get('/detalle/{id?}', function ($id)  {
    $json = File::get("js/ganadores.json");
    $ganadores = json_decode($json, true);

    $retorno = array();
    foreach ($ganadores as $key => $ganador) {
        if($ganador['id'] == $id )
            $retorno = $ganador;
    }

    return view('detail')->with(['ganador'=> $retorno]);;
});

function reemplazar($string){
        $string = trim($string);
 
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );
    return strtolower($string);
}

Auth::routes();


