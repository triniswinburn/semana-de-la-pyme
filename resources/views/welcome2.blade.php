<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Semana de la Pyme</title>
        <!-- Search Engine -->
        <meta name="description" content="¡Vota por la Selección Nacional de Pymes!">
        <!-- Schema.org for Google -->
        <meta name="keywords" content="Semana de la Pyme, pyme, Selección Nacional de Pymes" />
        <meta itemprop="name" content="Semana de la Pyme">
        <meta itemprop="description" content="¡Vota por la Selección Nacional de Pymes!">

        <!-- Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="Semana de la Pyme">
        <meta name="twitter:description" content="¡Vota por la Selección Nacional de Pymes!">
        <meta name="twitter:image" content="https://www.semanadelapyme.cl/images/semanadelapyme.jpg" />
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta property="og:title" content="Semana de la Pyme">
        <meta property="og:description" content="¡Vota por la Selección Nacional de Pymes!">
        <meta property="og:type" content="website">
        <meta property="og:url" content="https://www.semanadelapyme.cl/" />
        <meta property="og:site_name" content="Semana de la Pyme" />
        <meta property="og:keywords" content="Semana de la Pyme, pyme, Selección Nacional de Pymes" />
        <meta property="og:image" content="https://www.semanadelapyme.cl/images/semanadelapyme.jpg" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <!-- GOOGLE FONT -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
        <!-- FAVICON -->
        <link rel="apple-touch-icon" sizes="57x57" href="https://cdn.digital.gob.cl/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="https://cdn.digital.gob.cl/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="https://cdn.digital.gob.cl/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="https://cdn.digital.gob.cl/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="https://cdn.digital.gob.cl/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="https://cdn.digital.gob.cl/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="https://cdn.digital.gob.cl/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="https://cdn.digital.gob.cl/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="https://cdn.digital.gob.cl/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="https://cdn.digital.gob.cl/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="https://cdn.digital.gob.cl/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="https://cdn.digital.gob.cl/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="https://cdn.digital.gob.cl/favicon/favicon-16x16.png">
        <link rel="manifest" href="https://cdn.digital.gob.cl/favicon/manifest.json">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-48790041-1"></script>
        <script>window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-48790041-1');</script>
    </head>
    <body>
    <!-- HEADER -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="https://www.gob.cl/" target="_blank"><img src="images/logo.png" width="150" class="d-inline-block align-top img-fluid" alt="" style="margin-top: -35px;"></a>        
    </nav>
    <!-- FIN -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid justify-content-center">
        <img src="images/title.png" class="img-fluid title" alt="" width="500px">
        <img src="images/logo-landing.png" class="img-fluid logo-landing" alt="" width="500px">
    </div>
    <!-- FIN -->

    <br><br>
    <div class="container text-center">
        <a href="/" class="btn btn-light btn-lg" role="button" aria-pressed="true" style="color: #2d5568;"><i class="fas fa-arrow-left"></i>&nbsp Volver a la cancha</a>
    </div>
    <br><br>

    <!-- TEXTO -->
    <div class="row scrollToHere" style="color: white;">
        <div class="container text-left">
            <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                      <div class="input-group-append">
                        <span class="input-group-text input-group-desc">Selecciona por:</span>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-8">
                        {{ Form::open(array('url' => '/participantes', 'method' => 'POST')) }}
                        <select id="listaregiones" name="listaregiones" class="custom-select" onchange='this.form.submit()'>
                            <option value="00" seleted>Región</option>
                            <option value="00">Todos</option>
                            <option value="15">Arica y Parinacota</option>
                            <option value="01">Tarapacá</option>
                            <option value="02">Antofagasta</option>
                            <option value="03">Atacama</option>
                            <option value="04">Coquimbo</option>
                            <option value="05">Valparaíso</option>
                            <option value="13">Metropolitana de Santiago</option>
                            <option value="06">Libertador General Bernardo O’Higgins</option>
                            <option value="07">Maule</option>
                            <option value="16">Ñuble</option>
                            <option value="08">Bio Bio</option>
                            <option value="09">Araucanía</option>
                            <option value="14">Los Ríos</option>
                            <option value="10">Los Lagos</option>
                            <option value="11">Aysén del General Carlos Ibáñez del Campo</option>
                            <option value="12">Magallanes y de la Antártica Chilena</option>
                        </select>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-7 col-lg-7">
            {{ Form::open(array('url' => '/participantes', 'method' => 'POST')) }}
               <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="input-group-append">
                            <span class="input-group-text input-group-desc">o busca a tu favorito:</span>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-10 col-lg-10"  style="padding: 0px !important;">
                                <div class="input-group">
                                    <input type="text" name="buscar" class="form-control col-xs-12 col-md-8 col-lg-8" placeholder="Ej. Panadería El mejor pan" aria-label="Ej. Panadería 'El mejor pan'" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2">
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit" style="border-radius: 5px; background-color:#447085; margin-left: -15px;">LIMPIAR</button>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>

            {{ Form::close() }}
            </div>            
        </div>
    </div>
    <!-- FIN -->

    <!-- CARDS -->
    <div class="row">
        <div class="container">
            <div class="card-deck">
                <div class="row videos">
                    @foreach ($videos as $video)
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 caja-video region{{$video['id_region']}}">
                        <div class="card-body text-left">
                            <div class="contenedor">
                              <div class="reproductor" data-id="{{$video['youtube']}}"></div>
                            </div>
                            <br>
                            <h6>{{$video['nombre']}}</h6>
                            <h6>{{$video['region']}}</h6>
                            <p class="card-text">{{$video['descripcion']}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- FIN -->

    <br><br>
    <!-- TEXTO -->
    <div id="notfound" class="row hidden notfound" style="color: white;">
        <div class="container text-center">
            <div class="col-12">
                <img src="images/notfound.png" class="img-fluid" alt="" width="200px">
                <br>
                <h3><b>¡No hay resultados!</b></h3>
                <h3>Por favor, realiza otra búsqueda.</h3>
            </div>
        </div>
    </div>
    <!-- FIN -->
    <!-- CARGAR MAS -->
    <div class="row btn-mas-videos">
        <div class="container">
            <div class="col-12 text-center">
                    <button id="load" type="button" class="btn btn-outline-light"><i class="fas fa-plus"></i> &nbsp Cargar más videos</button>
            </div>
        </div>
    </div>
    <!-- FIN -->
    <br><br>
    <!-- TEXTO -->
    <div class="row">
        <div class="container">
            <div class="col-12 text-center">
                <a href="https://twitter.com/search?f=tweets&vertical=default&q=%23semanadelapyme&src=typd" target="_blank"><h1 style="color: white;"><strong class="hashtag">#SemanaDeLaPyme</strong></h1></a>
                <br>
                <a href='#' id='arriba' style="color: white;">Volver arriba &uarr;</a>
            </div>
        </div>
    </div>
    <!-- FIN -->
    <br><br>

    <!-- FOOTER -->
    <footer class="footer">
        <div class="container">
            <div class="col-6 col-md-9 col-lg-9">
                <a href="https://gob.cl">
                    <img src="images/logo.png" width="150px" alt="" style="margin-top: -32px;">
                </a>
            </div>
            <div class="col-6 col-md-3 col-lg-3">
                <div class="col-4 col-md-4">
                    <a href="https://www.twitter.com/Gobiernodechile" target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                </div>
                <div class="col-4 col-md-4">
                    <a href="https://www.facebook.com/gobiernodechile" target="_blank">
                        <i class="fab fa-facebook"></i>
                    </a>
                </div>
                <div class="col-4 col-md-4">
                    <a href="https://www.instagram.com/gobiernodechile" target="_blank">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>
        </div>
    </footer>
    <!-- FIN -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            (function() {
                var v = document.getElementsByClassName("reproductor");
                for (var n = 0; n < v.length; n++) {
                    var p = document.createElement("div");
                    p.innerHTML = labnolThumb(v[n].dataset.id);
                    p.onclick = labnolIframe;
                    v[n].appendChild(p);
                }
            })();
            function labnolThumb(id) {
                return '<img class="imagen-previa" src="//i.ytimg.com/vi/' + id + '/hqdefault.jpg"><div class="youtube-play"></div>';
            }
            function labnolIframe() {
                var iframe = document.createElement("iframe");
                iframe.setAttribute("src", "//www.youtube.com/embed/" + this.parentNode.dataset.id + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=0&showinfo=0");
                iframe.setAttribute("frameborder", "0");
                iframe.setAttribute("id", "youtube-iframe");
                this.parentNode.replaceChild(iframe, this);
            }

            @if(count($videos)==0)
                $('#notfound').removeClass('hidden');
                $('#load').addClass('hidden');
            @else
                $('#notfound').addClass('hidden');
                $('#load').removeClass('hidden');
            @endif
            @if(count($videos) <= 8)
                $('#load').addClass('hidden');
            @else
                $('#load').removeClass('hidden');
            @endif

            @if($buscar != "" || $region != "" )
                $('html,body').animate({
                    scrollTop: $(".scrollToHere").offset().top
                }, 500);
            @endif
        });
    </script>
    <script src="{{ asset('js/utils.js') }}"></script>
    </body>
</html>
