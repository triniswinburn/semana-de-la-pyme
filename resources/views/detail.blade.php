<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Semana de la Pyme</title>
        <!-- Search Engine -->
        <meta name="description" content="¡Vota por la Selección Nacional de Pymes!">
        <!-- Schema.org for Google -->
        <meta name="keywords" content="Semana de la Pyme, pyme, Selección Nacional de Pymes" />
        <meta itemprop="name" content="Semana de la Pyme">
        <meta itemprop="description" content="¡Vota por la Selección Nacional de Pymes!">

        <!-- Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="Semana de la Pyme">
        <meta name="twitter:description" content="¡Vota por la Selección Nacional de Pymes!">
        <meta name="twitter:image" content="https://www.semanadelapyme.cl/images/semanadelapyme.jpg" />
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta property="og:title" content="Semana de la Pyme">
        <meta property="og:description" content="¡Vota por la Selección Nacional de Pymes!">
        <meta property="og:type" content="website">
        <meta property="og:url" content="https://www.semanadelapyme.cl/" />
        <meta property="og:site_name" content="Semana de la Pyme" />
        <meta property="og:keywords" content="Semana de la Pyme, pyme, Selección Nacional de Pymes" />
        <meta property="og:image" content="https://www.semanadelapyme.cl/images/semanadelapyme.jpg" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <!-- GOOGLE FONT -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
        <!-- FAVICON -->
        <link rel="apple-touch-icon" sizes="57x57" href="https://cdn.digital.gob.cl/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="https://cdn.digital.gob.cl/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="https://cdn.digital.gob.cl/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="https://cdn.digital.gob.cl/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="https://cdn.digital.gob.cl/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="https://cdn.digital.gob.cl/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="https://cdn.digital.gob.cl/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="https://cdn.digital.gob.cl/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="https://cdn.digital.gob.cl/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="https://cdn.digital.gob.cl/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="https://cdn.digital.gob.cl/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="https://cdn.digital.gob.cl/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="https://cdn.digital.gob.cl/favicon/favicon-16x16.png">
        <link rel="manifest" href="https://cdn.digital.gob.cl/favicon/manifest.json">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-48790041-1"></script>
        <script>window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-48790041-1');</script>
    </head>
    <body>
    <!-- HEADER -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="https://www.gob.cl/" target="_blank"><img src="/images/logo.png" width="150" class="d-inline-block align-top img-fluid" alt="" style="margin-top: -35px;"></a>        
    </nav>
    <!-- FIN -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid justify-content-center">
        <img src="/images/title.png" class="img-fluid title" alt="" width="500px">
        <img src="/images/logo-landing.png" class="img-fluid logo-landing" alt="" width="500px">
    </div>
    <!-- FIN -->

    <!-- DETALLE JUGADOR -->
    <div class="container" style="margin-bottom: 50px; color:white">
        <div class="row">
            <div class="col-md-12 text-left" style="margin-bottom: 50px;">
                <a href="/" class="btn btn-light btn-lg" role="button" aria-pressed="true" style="color: #2d5568;"><i class="fas fa-arrow-left"></i>&nbsp Volver a la selección</a>
            </div>
            <div class="col-md-4"><img src="/images/logos/{{$ganador['id']}}-min.png" width="300px"></div>
            <div class="col-md-8">
                <br><br><br>
                <h1>{{$ganador['nombre']}}</h1>
                <h5>{{$ganador['region']}}</h5>
                <p>{{$ganador['descripcion']}}</p>
            </div>
        </div>
    </div>
    <!-- FIN -->
    <!-- VIDEO -->
    <div class="container" style="padding: 0px;">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="{{$ganador['youtube']}}" allowfullscreen></iframe>
        </div>
    </div>
    <!-- FIN -->
    <br><br>
    <!-- TEXTO -->
    <div class="row">
        <div class="container">
            <div class="col-12 text-center">
                <a href="https://twitter.com/search?f=tweets&vertical=default&q=%23semanadelapyme&src=typd" target="_blank"><h1 style="color: white;"><strong class="hashtag">#SemanaDeLaPyme</strong></h1></a>
                <br>
                <a href='#' id='arriba' style="color: white;">Volver arriba &uarr;</a>
            </div>
        </div>
    </div>
    <!-- FIN -->
    <br><br>

    <!-- FOOTER -->
    <footer class="footer">
        <div class="container">
            <div class="col-6 col-md-9 col-lg-9">
                <a href="https://gob.cl">
                    <img src="images/logo.png" width="150px" alt="" style="margin-top: -32px;">
                </a>
            </div>
            <div class="col-6 col-md-3 col-lg-3">
                <div class="col-4 col-md-4">
                    <a href="https://www.twitter.com/Gobiernodechile" target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                </div>
                <div class="col-4 col-md-4">
                    <a href="https://www.facebook.com/gobiernodechile" target="_blank">
                        <i class="fab fa-facebook"></i>
                    </a>
                </div>
                <div class="col-4 col-md-4">
                    <a href="https://www.instagram.com/gobiernodechile" target="_blank">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>
        </div>
    </footer>
    <!-- FIN -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js" type="text/javascript"></script>
    <script>(function(d,e,s){if(d.getElementById("likebtn_wjs"))return;a=d.createElement(e);m=d.getElementsByTagName(e)[0];a.async=1;a.id="likebtn_wjs";a.src=s;m.parentNode.insertBefore(a, m)})(document,"script","//w.likebtn.com/js/w/widget.js");</script>
    </body>
</html>
